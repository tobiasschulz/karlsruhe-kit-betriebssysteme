#include <stdio.h>

#include "syscalls.h"

int main()
{
	/*
	 * Put your test code here.
	 */

	print3('a', 255, 127);
	write3('a', 255, 127);
	write3('a', 255, -120);
	write3('a', 10, -128);
	write3('a', 10, -127);

	printf("%s\n", getTime());

	return 0;
}
