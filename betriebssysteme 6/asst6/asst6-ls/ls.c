#define _GNU_SOURCE

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>


int compare(const void* p1, const void* p2) {
	return strcmp (*(char**)p1, *(char**)p2);
}

void print_dir(char** names) {
	for (size_t i = 0; names[i]; ++i) {
		struct stat st_buf;
		if (!stat (names[i], &st_buf)) {
			printf("%s %04o %s\n", S_ISDIR(st_buf.st_mode) ? "d" : S_ISREG(st_buf.st_mode) ? "f" : "?", st_buf.st_mode & 07777, names[i]);
		}
	}
}

char* path(char* parent, char* child) {
	char* path = calloc(strlen(parent)+1+strlen(child)+1, 1);
	strcpy(path, parent);
	strcat(path, "/");
	strcat(path, child);
	return path;
}

void ls(char* dirname, char*** names, size_t* size, size_t* capacity, size_t depth) {
	DIR* dir = opendir(dirname);
	if (dir == NULL) return;
	struct dirent* ent;
	while ((ent = readdir(dir)) != NULL) {
		if (strcmp(ent->d_name, ".") && strcmp(ent->d_name, "..")) {
			if (*capacity - *size < 5) {
				*names = realloc(*names, sizeof(char*)*(*capacity*=2));
			}
			char* filename = path(dirname, ent->d_name);
			(*names)[(*size)++] = filename;
			struct stat st_buf;
			if (!stat (filename, &st_buf) && S_ISDIR(st_buf.st_mode) && depth <= 0xff) {
				ls(filename, names, size, capacity, depth+1);
			}
		}
	}
	closedir(dir);
}

int main() {
	size_t capacity = 10;
	size_t size = 0;
	size_t depth = 0;
	char** names = malloc(sizeof(char*)*capacity);

	ls(".", &names, &size, &capacity, depth);

	names[size++] = 0;

	qsort(names, size-1, sizeof(char *), compare);
	print_dir(names);

	for (size_t i = 0; names[i]; ++i) {
		free(names[i]);
	}
	free(names);

	return 0;
}

