#!/usr/bin/perl

use strict;
use warnings;

my $MEM_SIZE = 400;
my $POLICY = 'BEST_FIT';

my @partitions = ();

sub init_allocator {
	@partitions = ({ id=>0, base=>0, limit=>$MEM_SIZE })
}
sub cleanup_allocator {
	@partitions = ()
}
sub allocate_in_null_partition {
	my ($nullblock, $size, $id) = @_;
	@partitions = grep {$_->{limit}!=$_->{base}} map { $_ == $nullblock ? (({id=>$id, base=>$_->{base},limit=>$size}, {id=>0, base=>$size+1, limit=>$_->{limit}})) : $_ } @partitions;
}
sub allocate_partition {
	my ($size, $id) = @_;
	$POLICY eq "FIRST_FIT" and do { return allocate_in_null_partition($_, $size, $id) if $_->{id}==0 && $_->{limit}-$_->{base} >= $size } foreach (@partitions);
	return allocate_in_null_partition((sort {$a->{limit}-$a->{base}-$size cmp $b->{limit}-$b->{base}-$size} grep {$_->{id}==0 && $_->{limit}-$_->{base} >= $size} @partitions)[$POLICY eq "BEST_FIT" ? 0 : -1], $size, $id);
}
sub release_partition {
	my ($addr) = @_;
	my $tmp = $MEM_SIZE;
	@partitions = reverse map { $_->{limit} = $tmp; $tmp = $_->{base} and $_->{id} = 0 if $tmp != $_->{base}; } reverse grep { $_->{base} != $addr } @partitions;
}
