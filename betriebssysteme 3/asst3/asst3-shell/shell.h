#ifndef _SHELL_H
#define _SHELL_H

char* readline();
char** split(char* input);

#endif
