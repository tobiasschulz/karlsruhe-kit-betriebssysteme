#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "shell.h"

// read a line from standard input and return a dynamically allocated buffer
char* readline() {
    int capacity = 256;
    char* buffer = (char*)malloc(capacity+2);

    char character;
    int i;
    for (i = 0; (character = fgetc(stdin)) != '\n'; ++i) {
        if (character == EOF) {
            free(buffer);
            return 0;
        }
        if (i >= capacity) {
            capacity *= 2;
            buffer = realloc(buffer, capacity+2);
        }

        buffer[i] = character;
    }
    buffer[i] = '\0';

    return buffer;
}

// split a string by spaces and return a dynamically allocated string array
char** split(char* input) {
    int capacity = 2;
    char** argv = (char**)calloc(capacity+1, sizeof(char*));

    char* token = strtok(input, " \t\n\r");
    int i;
    for (i = 0; token != NULL; i++) {
        if (i >= capacity) {
            capacity *= 2;
            argv = realloc(argv, (capacity+1)*sizeof(char*));
        }

        argv[i] = malloc(strlen(token)+1);
        strcpy(argv[i], token);
        token = strtok(NULL, " \t\n\r");
    }
    argv[i] = NULL;
    return argv;
}

int main()
{
    while (1) {
        printf("$ ");

        char* input = readline();
        // break if end of line or if input matches "exit"
        if (input == NULL)
            break;
        if (strcmp(input, "exit") == 0) {
            free(input);
            break;
        }
        // continue if line is empty
        if (strlen(input) == 0) {
            free(input);
            continue;
        }

        char** argv = split(input);

        // fork
        int pid = fork();
        if (pid < 0) {
            printf("Error: unable to fork...\n");
            return 1;
        }

        if (pid) {
            // wait for the process to complete
            time_t start = time(NULL);
            int status;
            waitpid(pid, &status, 0);
            time_t stop = time(NULL);

            // print run time and status
            int runtime = (int)stop - (int)start;
            int hours = runtime / 3600;
            int minutes = (runtime / 60) % 60;
            int seconds = runtime % 60;
            printf("run time: %02d:%02d:%02d\nexit status: %d\n", hours, minutes, seconds, status);

            // free buffers
            free(input);
            int i;
            for (i = 0; argv[i] != NULL; ++i)
                free(argv[i]);
            free(argv);
        }
        else {
            execv(argv[0], argv);
            printf("Error: unable to run '%s'...\n", argv[0]);
            return 0;
        }
    }

    return 0;
}
