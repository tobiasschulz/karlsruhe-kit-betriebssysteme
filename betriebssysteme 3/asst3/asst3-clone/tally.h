#ifndef TALLY_H
#define TALLY_H

void tally_init ();
void increment ( uint64_t *tally, uint64_t steps );
void tally_clean ();

#endif  /* ndef TALLY_H */

