#define _GNU_SOURCE

#include <sys/time.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>

uint64_t lasttime = 0;

void handler_for_sigint(int useless) {
	(void) useless;

	struct timeval tv;
	gettimeofday(&tv, NULL);

	uint64_t microseconds = tv.tv_sec;
	microseconds *= 1000*1000;
	microseconds += tv.tv_usec;

	printf("Received signal\n");

	if (microseconds - lasttime <= 2000000) {
		printf("Shutting down\n");
		exit(1);
	}
	else {
		lasttime = microseconds;
	}
}


int main( void )
{
	struct sigaction signal_action_struct;
	memset(&signal_action_struct, 0, sizeof signal_action_struct);
	signal_action_struct.sa_handler = &handler_for_sigint;

	int error = sigaction(SIGINT, &signal_action_struct, NULL);
	if (error) {
		fprintf(stderr, "Error in sigaction!");
	}

	printf( "Hello World!\n" );
	for ( ;; )
	{
		/* infinite loop */
	}

	return 0;
}
