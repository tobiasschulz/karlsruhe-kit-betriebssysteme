#ifndef DATASTRUCT_H
#define DATASTRUCT_H

/* insert process in data structure */
void insert ( int id, int priority );

/* return element with highest priority */
int front ();

/* remove element with highest priority */
void pop ();

/* remove element with given id */
void popId ( int id );

#endif
