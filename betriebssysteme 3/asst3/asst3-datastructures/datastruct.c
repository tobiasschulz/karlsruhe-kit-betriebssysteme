#include <stdlib.h>
#include <stdio.h>
#include "datastruct.h"

typedef struct process_list {
	struct process_list *next;
	int id;
	int prio;
} process_list;

struct process_list p;

struct process_list begin = { &begin, -1, -1 };

/* insert process in data structure */
void insert ( int id, int priority ) 
{
	process_list* newprocess = malloc(sizeof(process_list));
	process_list* current = &begin;

	newprocess->prio = priority;
	newprocess->id = id;

	while (current->next->prio >= priority) {
		current = current->next;
	}

	newprocess->next = current->next;
	current->next = newprocess;
}


/* return element with highest priority */
int front ()
{
	return begin.next->id;
}


/* remove element with highest priority */
void pop ()
{
	if (begin.next->id == -1) {
		return;
	}
	else {
		process_list* todelete = begin.next;
		begin.next = todelete->next;
		free(todelete);
	}
}


/* remove element with given id */
void popId ( int id )
{
	process_list* current = &begin;

	while (id != current->next->id) {
		current = current->next;
	}

	process_list* element = current->next;
	current->next = element->next;
	free(element);
}
