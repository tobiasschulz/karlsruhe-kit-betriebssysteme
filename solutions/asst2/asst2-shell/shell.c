#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "mem.h"
#include "shell.h"

#define INITIAL_LINE_BUF_SIZE 1024
#define PROMPT "$ "

/*
 * General notes:
 *
 * - In my C programs, every function returns an error value.
 *   This has turned out to be an efficient and consistent
 *   approach for error handling in my projects. There are some
 *   macros defined to make error handling rather non-verbose
 *   (better not bloat up our code to check for errors if it can be
 *   be done much cleaner using a small set of simple macros).
 *   I like a low indentation level.
 * - libc's memory management is wrapped using a simple set of functions
 *   to make it integrate well with our error handling system
 *   and to prevent common errors like double-free. This makes
 *   allocating and freeing memory (almost) a pleasure :)
 * - file-local functions are not declared in header files,
 *   unless this is inevitable. Makes refactoring easier.
 */

/**
 * Parses a shell command line by splitting it into parts seperated
 * by whitespace. Does NOT support quoted strings!
 *
 * @param cmdline The command line to parse
 * @param argv    Pointer to a char** pointer that receives the
 *                parsed argument vector
 *
 * @return error status
 */
error_t parse_argv(char *cmdline, char ***argv)
{
  char *arg;
  size_t i;
  INIT_STATUS_SUCCESS;

  // check input pointers
  if (!argv)
    return E_INVALID_ARG;

  // reserve space (will be expanded later if needed)
  TRY(my_calloc((void**)argv, 10*sizeof(char*)));

  // start tokenizing cmdline
  arg = strtok(cmdline, " \t");
  for (i = 0; arg; ++i) {
    // re-allocate arg vector (size i+2, because we
    // need to NULL-terminate the array of pointers)
    TRY_BREAK(my_realloc((void**)argv, (i+2) * sizeof(char*)));

    // null-terminate
    (*argv)[i] = NULL;

    // save current token/argument
    TRY_BREAK(my_alloc((void**)&(*argv)[i], strlen(arg)+1));
    strcpy((*argv)[i], arg);

    // read next token
    arg = strtok(NULL, " ");
  }

  if (status) {
    // clean up if an error occured
    free_argv(argv);
    return status;
  }

  return E_SUCCESS;
}

/**
 * Frees an argument vector structure created by the
 * parse_argv function.
 *
 * @param argv A pointer to the argument vector to be freed.
 *
 * @return error status
 */
error_t free_argv(char ***argv)
{
  // free every argument
  for (char **arg = *argv; *arg; ++arg) {
    my_free((void**)arg);
  }

  // free list of pointers to arguments
  return my_free((void**)argv);
}

/**
 * Reads a line from a stream and return it in a malloc'ed buffer.
 * It is the caller's responsibility to free the returned buffer.
 *
 * @param stream      The stream from which to read
 * @param line_buffer Pointer to a pointer which will receive
 *                    the null-terminated C string
 *
 * @return error status
 */
error_t read_line(FILE *stream, char **line_buffer)
{
  char c;
  size_t i = 0;
  INIT_STATUS_SUCCESS;

  // check input pointer
  if (!line_buffer)
    return E_INVALID_ARG;

  // reserve space for command line
  TRY(my_alloc((void**)line_buffer, INITIAL_LINE_BUF_SIZE));
  while ((c = fgetc(stream)) != '\n') {
    if (c == EOF) {
      // which error occured?
      if (feof(stream))
        return E_EOF;
      else
        // otherwise, a real error occured => fail
        return E_STDLIB_ERROR;
    }

    // reserve one byte more for null-termination
    TRY_BREAK(my_realloc((void**)line_buffer, i+2));

    // save byte into buffer and increment index counter
    (*line_buffer)[i++] = c;
  }

  if (status) {
    // clean up if an error occured
    my_free((void**)line_buffer);
    return status;
  }

  // null-terminate
  (*line_buffer)[i] = '\0';

  return E_SUCCESS;
}

/**
 * Implements a simple shell with the following features:
 *  - No restriction on command line length
 *  - Allow running programs with command line arguments
 *    (whitespace-separated)
 *  - Examine exit status of subprocesses
 *  - Examine run time of subprocesses
 */
int main()
{
  char *line_buffer;
  char **argv;
  pid_t child_pid;
  int exit_code;
  time_t start_time, run_time;
  INIT_STATUS_SUCCESS;

  // infinite input loop
  for (;;) {
    // print prompt
    printf(PROMPT);
    fflush(stdout);

    // read command line (up to newline)
    status = read_line(stdin, &line_buffer);
    if (status != E_SUCCESS && status != E_EOF) {
      fprintf(stderr, "Error while reading a line.\n");
      return 1;
    }

    // "exit" command and ctrl+D terminate our shell
    if (status == E_EOF)
      printf("\n"); // print newline before exiting
    if (status == E_EOF || !strcmp("exit", line_buffer)) {
      // exit gracefully
      return 0;
    }

    // parse command line
    TRY(parse_argv(line_buffer, &argv));

    // empty command line?
    if (!(*argv))
      continue;

    // execute the user command
    child_pid = fork();
    if (child_pid < 0) {
      fprintf(stderr, "Error while forking!\n");
      return 3;
    }

    if (!child_pid) {
      // child has to work
      execv(argv[0], argv);
      fprintf(stderr, "Error while executing program '%s'.\n", argv[0]);
      return 0;
    } else {
      start_time = time(NULL);
      // parent has to wait
      waitpid(child_pid, &exit_code, 0);
      run_time = time(NULL) - start_time;
      printf("Child ran %02d:%02d:%02d and exited with status: %d\n",
             (int)(run_time / 3600),
             (int)((run_time / 60) % 60),
             (int)(run_time % 60),
             exit_code);
    }

    // clean up
    my_free((void**)&line_buffer);
    free_argv(&argv);
  }

	return 0;
}
