#define _GNU_SOURCE

#include <stdbool.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "../common/def.h"

#define RECURSION_LEVEL 255

int remove_dangling_symlinks(size_t depth, bool *keep_dir, size_t *actions) {
  if (depth > RECURSION_LEVEL) return EXIT_FAILURE;

  char *wd = get_current_dir_name();
  if (!wd) PERROR_RETURN("getcwd failed");

  DIR *dir = opendir(".");
  struct dirent *entry;
  struct stat statbuf;
  bool keep_subdir;
  while ((entry = readdir(dir))) {
    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
      continue;

    if (lstat(entry->d_name, &statbuf)) PERROR_RETURN("lstat failed");

    if (S_ISDIR(statbuf.st_mode)) {
      // recurse
      //printf("recursing into subdirectory %s\n", entry->d_name);

      if (chdir(entry->d_name)) PERROR_RETURN("chdir failed");

      keep_subdir = false;
      // propagate errors, we don't want to take any risk of deleting
      // the wrong files!
      if (remove_dangling_symlinks(depth + 1, &keep_subdir, actions))
        return EXIT_FAILURE;

      if (chdir(wd)) PERROR_RETURN("chdir failed");

      // post-process
      if (keep_subdir)
        *keep_dir = true;
      else {
        printf("deleting subdir %s\n", entry->d_name);
        if (rmdir(entry->d_name)) PERROR_RETURN("rmdir failed");
        ++(*actions);
      }
      continue;
    }

    if (!S_ISLNK(statbuf.st_mode)) {
      *keep_dir = true;
      continue;
    }

    // we found a symlink, check whether the target exists
    int ret = stat(entry->d_name, &statbuf);
    if (!ret || (errno != ENOENT && errno != ENOTDIR)) {
      // symlink okay
      *keep_dir = true;
      continue;
    }

    // bah
    printf("removing link %s/%s\n", wd, entry->d_name);
    if (unlink(entry->d_name)) PERROR_RETURN("unlink failed");
    ++(*actions);
  }

  free(wd);
  return EXIT_SUCCESS;
}

int main()
{
  bool keep;
  size_t actions = 1;

  // there is a small issue that can cause false negatives in the above function:
  // If a link points to a directory which will be deleted during the
  // process, it depends on the order in which readdir returns the entries,
  // whether the link will be detected as dangling or not. To fix this, we
  // could process the directories first, but this seems like a lot of
  // effort. Instead, we just repeat the whole process until no more removals are
  // performed (this may also help in case of cyclic dependencies).
  while (actions) {
    actions = 0;
    remove_dangling_symlinks(0, &keep, &actions);
    // don't delete working directory, so we don't use keep here
  }
}
