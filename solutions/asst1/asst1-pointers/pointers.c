#include "pointers.h"

int average ( int *arrayPointer, unsigned int size )
{
  int sum = 0;

  for (unsigned int i = 0; i < size; ++i)
    sum += arrayPointer[i];

  // need cast to perform a signed division
	return sum / (int)size;
}


int p_average ( int **pointerArray, unsigned int size )
{
  int array[size];

  // build up new array so we can reuse the "average" function
  for (unsigned int i = 0; i < size; ++i)
    array[i] = *pointerArray[i];
  return average(array, size);
}
