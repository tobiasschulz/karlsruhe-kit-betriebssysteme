	.text                         ; store the following code in .text section of the resulting executable
                                ; (this section allows reading and executing access by default, but cannot
                                ; be written to)

square:                         ; function "square" starts here
  ; stack frame setup
  ; ----------------------
	pushl	%ebp                    ; push old frame pointer onto the stack,
                                ; so it can be restored before return
	movl	%esp, %ebp              ; use the current stack pointer as our function's
                                ; frame pointer. Frame layout after this instruction:
                                ;
                                ;   Location   Value
                                ;   8(%ebp)    argument "x" (32-bit uint)
                                ;   4(%ebp)    return address
                                ;   0(%ebp)    old frame pointer
                                ;
	subl	$16, %esp               ; reserve 16 bytes of stack space for local variables.
                                ; Frame is extended by the following locations:
                                ;
                                ;   -4(%ebp)   high DWORD of local 64-bit variable "res"
                                ;   -8(%ebp)   low DWORD of local 64-bit variable "res"
                                ; The other 8 bytes are reserved for stack pointer alignment
                                ; (actually not very sensible here)
  ; -----------------------

	movl	8(%ebp), %eax           ; copy value of argument "x" into %eax
	imull	8(%ebp), %eax           ; multiply %eax with "x" (with itself), result goes in %edx:%eax
	movl	$0, %edx                ; zero out %edx (so we lose the upper 4 bytes of the calculation,
                                ; because we forgot to cast to 64-bit ints before multiplication)
                                ; After this, %edx:%eax contains the the calculated value of "res"

  ; the following four instruction could be safely removed for optimization
	movl	%eax, -8(%ebp)          ; save low  DWORD of the product in the local variable "res"
	movl	%edx, -4(%ebp)          ; save high DWORD of the product in the local variable "res
	movl	-8(%ebp), %eax          ; save low  DWORD of return value in %eax (cdecl convention)
	movl	-4(%ebp), %edx          ; save high DWORD of return value in %edx (cdecl convention)

  ; stack frame restauration
  ; ----------------------
	leave                         ; equivalent to "movl %ebp, %esp; popl %ebp":
                                ;    1. tear down stack frame by restoring the old stack pointer
                                ;       from the frame pointer
                                ;    2. restore the old frame pointer (which lives on top of the stack now)
  ; -----------------------

	ret                           ; pop return address from stack and jump to it (return to caller)

main:                           ; function "main" starts here

  ; stack frame setup
  ; ----------------------
	pushl	%ebp                    ; save old frame pointer on the stack (see "square" function for expl.)
	movl	%esp, %ebp              ; use the current stack pointer as frame pointer (see "square" function
                                ; annotations for stack layout after this)
	andl	$-8, %esp               ; align stack pointer to 8 bytes
	subl	$24, %esp               ; reserve 24 bytes of stack space for local variables
  ; -----------------------

	movl	$5, 20(%esp)            ; save value $5 in local variable "y" (32-bit uint)
	movl	20(%esp), %eax          ; read variable "y" into %eax
	movl	%eax, (%esp)            ; save first parameter for square function (y=5) on top of the stack
                                ; according to cdecl calling convention (stack pointer is not changed here!)
	call	square                  ; push instruction pointer onto the stack and jump to the "square" function
	movl	%eax, 16(%esp)          ; save return value of "square" function (which lives in %eax, according to
                                ; the cdecl convention) to local variable "z"
	movl	$0, %eax                ; save the return value 0 of our "main" function in %eax

  ; stack frame restauration
  ; ----------------------
	leave                         ; restore stack pointer, then restore old frame pointer from stack
                                ; (see "square" function)
  ; -----------------------

	ret                           ; pop return address from stack and jump to it (return to caller,
                                ; probably some libc function)
