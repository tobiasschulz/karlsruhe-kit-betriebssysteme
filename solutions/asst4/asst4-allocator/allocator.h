#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <stdint.h>

#define MEM_SIZE 400

/* constants that designate an allocation policy */
#define FIRST_FIT 0
#define BEST_FIT  1
#define WORST_FIT 2

/* change to develop and test different policies */
#define POLICY FIRST_FIT
//#define POLICY BEST_FIT
//#define POLICY WORST_FIT

int init_allocator(); /* initialize memory allocator;
 * returns 0 on success, -1 otherwise */

void cleanup_allocator(); /* drop all allocations, release all memory;
 * the cleanup function should reset your allocation system, so that it
 * can be re-initialized (e.g., to test another policy) */


void * allocate_partition(unsigned int size, char id); /* allocate a
 * partition with the specified size and identifying letter */

void release_partition(void * addr); /* release a partition;
 * must use the (imaginary) addresses returned by allocate_partition as
 * arguments */

void print_memory_map(); /* print current allocations as a memory map */

#endif
