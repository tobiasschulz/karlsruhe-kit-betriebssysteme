#include <stdio.h>
#include "allocator.h"

int main()
{
	/* example from assignment */
	init_allocator();
  void *foo1 = allocate_partition(4, 'A');
  allocate_partition(3, 'B');
  void *foo2 = allocate_partition(2, 'C');
  allocate_partition(1, 'D');
  allocate_partition(388, 'F');

  release_partition(foo1);
  release_partition(foo2);
  allocate_partition(1, 'E');

	print_memory_map();

	return 0;
}
