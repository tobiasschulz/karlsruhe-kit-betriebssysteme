#ifndef SEGMENTS_H
#define SEGMENTS_H

#include <inttypes.h>

struct segment
{
	uint16_t base;
	uint16_t limit;
};

/* the following functions determine
	* the segment number,
	* the offset,
	* the validity of the logical address,
	* the physical address (or 0 for invalid addresses)
   from a logical address, based on the segment table defined above
*/
int segment_number( uint16_t addr );
int offset( uint16_t addr );
int isValid( uint16_t addr );
int physical_addr( uint16_t addr );

#define NSEGMENTS 8

#endif
