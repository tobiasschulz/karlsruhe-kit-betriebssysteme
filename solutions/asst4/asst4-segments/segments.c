#include <stddef.h>

#include "segments.h"

struct segment segment_table[NSEGMENTS];

#define SEGMENT_BITS 3

int segment_number( uint16_t addr )
{
  // first SEGMENT_BITS bits hold segment#,
  // so we shift to the right
	return addr >> (16 - SEGMENT_BITS);
}

int offset( uint16_t addr )
{
  // cut the most significant SEGMENT_BITS bits
  return ((addr << SEGMENT_BITS) & 0xffff) >> SEGMENT_BITS;
}

int isValid( uint16_t addr )
{
  return segment_table[segment_number(addr)].limit
           > offset(addr);
}

/**
 * Returns the physical address or 0 on error
 */
int physical_addr( uint16_t addr )
{
  if (!isValid(addr))
    return 0; // signal error

  return segment_table[segment_number(addr)].base
           + offset(addr);
}

