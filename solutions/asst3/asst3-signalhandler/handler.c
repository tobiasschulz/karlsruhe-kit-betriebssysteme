#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/time.h>

#define FAIL_GRACELESSLY(msg) { perror(msg); exit(1); }

uint64_t time_last_signal = 0;

void sigint_handler(int unused)
{
  (void)unused;

  struct timeval now;
  uint64_t time;
  gettimeofday(&now, NULL);
  time = now.tv_sec*1000000 + now.tv_usec;

  printf("Received signal.\n");

  if (time - time_last_signal <= 2000000) {
    printf("Bye.\n");
    exit(0);
  }
  time_last_signal = time;
}

void setup_sigint_handler()
{
  // Is -Werror really necessary?
  // I know what I'm doing here and its valid C99,
  // yet I can't do it because of -Werror.
  //struct sigaction action = {0};
  struct sigaction action;
  memset(&action, 0, sizeof action);

  action.sa_handler = &sigint_handler;
  if (sigaction(SIGINT, &action, NULL) < 0)
    FAIL_GRACELESSLY("sigaction failed");
}

int main( void )
{
  setup_sigint_handler();

	printf( "Hello World!\n" );
	for ( ;; )
	{
		sleep(1); // avoid busy loop, sleep will be
              // interrupted by SIGINT
	}

	return 0;
}
