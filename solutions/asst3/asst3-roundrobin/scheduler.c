#define _GNU_SOURCE

#include <stddef.h> /* for NULL */

#include "scheduler.h"
#include "ult.h"

/* Select and return the thread that should be executed next.
 * Use the following data-structure
 * - threads: "thread control block" array which contains the state
 *    i.e.: threads[4]->state will return the current state
 *    of thread 4
 *     - the available states are:
 *       terminated, ready, running and waiting (see ult.h)
 * - thread 0 is the idle thread!
 * - thread 1 up to number MAX_THREADS-1 are the worker threads
 */


// we need to duplicate this variable :(
int current_thread_id = 0;

int
schedule ()
{
  int i = current_thread_id;

  do {
    // calculate which thread we want to check now
    i = (i+1) % MAX_THREADS;

    if (i == 0 || !threads[i])
      // don't schedule idle thread or
      // not existing thread
      continue;

    if (threads[i]->state == running ||
        threads[i]->state == ready)
    {
      // nice, found one.
      current_thread_id = i;
      return i;
    }

  } while (i != current_thread_id);

  // no active thread found... select idle thread :(
  current_thread_id = 0;
  return 0;
}

