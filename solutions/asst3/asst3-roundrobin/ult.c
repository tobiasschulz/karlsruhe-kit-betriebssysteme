#define _GNU_SOURCE
#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#include "ult.h"
#include "scheduler.h"

#define STACK_SIZE 40960

#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)


extern struct Context *threads[MAX_THREADS];
static int currentThreadID = -1;


/* static prototypes */
static void dispatch ( int );
static void setTimer ();
static void timerTick ( int signum );
static void func ();
static void idleThread ();
static struct Context* newULT ( void (*func)() );
static void destroyULT ( struct Context *tcont );


static void
setTimer ()
{
	alarm( 1 );
}


// block thread tid for "time" ticks
int
block ( int tid, int time )
{
	if ( tid == 0 )
	{
		/* 0 is the idle thread and may not be blocked */
		errno = EINVAL;
		return -1;
	}

	// beware of the race-condition
	threads[tid]->state     = waiting;
	threads[tid]->blockTime += time;

#ifdef VERBOSE
	printf(" blocking %d for %d slots", tid, time );
#endif

	if ( tid == currentThreadID )
	{
		/* current thread is blocked and cannot continue */
#ifdef VERBOSE
		printf( "\n [sched] " ); fflush( stdout );
#endif
		manageBlockedThreads();
		dispatch( schedule() );
	}

	return 0;
}


// switch running ULT to the one with the given tid
static void
dispatch ( int tid )
{
	struct Context *oldThread = threads[currentThreadID];
	struct Context *newThread = threads[tid];

	setTimer();

	if( currentThreadID == tid )
	{
		/* no need to dispatch same thread again */
		return;
	}

#ifdef VERBOSE
	printf("switching threads %d -> %d ", currentThreadID, tid);
#endif

	oldThread->state = oldThread->state == waiting ? waiting : ready;
	newThread->state = running;
	currentThreadID = tid;

	if( swapcontext(&(oldThread->context), &(newThread->context)) == -1 )
	{
		handle_error( "swapcontext" );
	}
}


void
manageBlockedThreads ()
{
	for( int i = 0; i < MAX_THREADS; i++ )
	{
		if ( threads[i] == NULL || threads[i]->blockTime == 0 )
		{
			continue;
		}

		threads[i]->blockTime--;

		if( threads[i]->blockTime == 0 )
		{
			/* thread became ready during this tick */
			threads[i]->state = ready;
		}
	}
}


static void
timerTick ( int signum )
{
	(void) signum;
	manageBlockedThreads();

#ifdef VERBOSE
	printf( "\n [sched] " ); fflush( stdout );
#endif
	dispatch( schedule() );
}


/* this function is run by workers */
static void
func ()
{
	for( ;; )
	{
		if( (rand() % 100) < 5 )
		{
			block( currentThreadID, (rand() % 10) + 1 );
		}
		else
		{
#ifdef VERBOSE
			printf( "%d", currentThreadID );
			fflush( stdout );
#endif
			usleep( 100000 );
		}
	}
}


// this function is run by the idle thread only
static void
idleThread ()
{
	for( ;; )
	{
#ifdef VERBOSE
		printf( "0" );
		fflush( stdout );
#endif
		usleep( 100000 );
	}
}


static struct Context*
newULT ( void (*func)() )
{
	struct Context *tcont = malloc( sizeof(struct Context) );
	if ( tcont == NULL )
	{
		return NULL; /* malloc failed */
	}

	if( getcontext(&(tcont->context)) == -1 )
		handle_error( "getcontext" );

	tcont->state = ready;
	tcont->blockTime = 0;
	tcont->context.uc_stack.ss_size =  STACK_SIZE;
	if ( (tcont->context.uc_stack.ss_sp = malloc( STACK_SIZE )) == NULL )
	{
		free( tcont );
		return NULL; /* malloc failed */
	}

	makecontext( &(tcont->context), func, 0 );

	return tcont;
}


static void
destroyULT ( struct Context *tcont )
{
	free( tcont->context.uc_stack.ss_sp );
}


int
createThreadPool ( size_t numberOfWorkers )
{
	if ( numberOfWorkers > MAX_THREADS - 1 )
	{
		errno = EINVAL;
		return -1;
	}

	/* seed random number generator */
	srand( time(0) );

	// set up "timer interrupt" to call scheduler
	struct sigaction act;
	memset( &act, 0, sizeof act );
	act.sa_handler = &timerTick;
	sigaction( SIGALRM, &act, 0 );

	memset( threads, 0, sizeof threads );

	// create idle thread
#ifdef VERBOSE
	printf( " Creating new thread with tid %u\n", 0 );
#endif
	threads[0] = newULT( &idleThread );
	if ( threads[0] == NULL ) {
		return -1; /* allocation failed */
	}

	// set up worker threads
	for( size_t i = 1; i <= numberOfWorkers; i++ )
	{
#ifdef VERBOSE
		printf( " Creating new thread with tid %zu\n", i );
#endif
		threads[i] = newULT( &func );
		if ( threads[i] == NULL ) {
			destroyThreadPool();
			return -1; /* allocation failed */
		}
	}
	currentThreadID = -1;

	return 0;
}


void
destroyThreadPool ()
{
	for ( int i = 0; i < MAX_THREADS; i++ )
	{
		if ( threads[i] != NULL )
		{
			destroyULT( threads[i] );
			free( threads[i] );
			threads[i] = NULL;
		}
	}

	currentThreadID = -1;
}


int
runThreads ()
{
	if ( threads[0] == NULL )
	{
		/* thread pool not properly created */
		return -1;
	}

	// dispatch to first Thread (idle)
#ifdef VERBOSE
	printf( " Dispatching initial thread: " );
#endif
	// first tick
	setTimer();
	currentThreadID = 0;
	if( setcontext(&(threads[0]->context)) )
		handle_error( "setcontext" );

	return 0;
}
