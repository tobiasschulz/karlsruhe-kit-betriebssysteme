#define _GNU_SOURCE
#include <stdio.h>
#include <inttypes.h> /* for PRIu64 and uint64_t */

#include "tally.h"

#define THREADS 2

int main ()
{
	uint64_t N = 10000000;
	uint64_t tally = 0;

	tally_init();

	#pragma omp parallel for
	for( int i = 0; i < THREADS; i++ )
	{
		increment( &tally, N );
	}

	tally_clean();

	printf( "Tally is %" PRIu64 "\n", tally );
	return 0;
}

