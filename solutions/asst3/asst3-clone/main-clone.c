#define _GNU_SOURCE
#include <stdio.h>
#include <inttypes.h> /* for PRIu64 and uint64_t */
#include <stdlib.h>

#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>

#include "tally.h"

#define THREADS 2
#define STACK_SIZE 0xffff

#define FAIL_GRACELESSLY(msg) { perror(msg); exit(1); }

typedef struct
{
  uint64_t *tally;
  uint64_t steps;
} tally_context_s;

int unwrap_increment(void *param)
{
  tally_context_s *ctx = (tally_context_s*)param;
  increment(ctx->tally, ctx->steps);
  return 0;
}

int main ()
{
	uint64_t N = 10000000;
	uint64_t tally = 0;
  tally_context_s ctx;
  void *stacks[THREADS];
  int thread_ids[THREADS];

  // save context for function wrapper
  ctx.steps = N;
  ctx.tally = &tally;

	tally_init();

  // start threads
	for( int i = 0; i < THREADS; i++ )
	{
    // reserve stack space
    if (!(stacks[i] = malloc(STACK_SIZE)))
      FAIL_GRACELESSLY("malloc failed");

    thread_ids[i] = clone(&unwrap_increment,
                          stacks[i] + STACK_SIZE, // stack grows downwards!
                          CLONE_VM | CLONE_FILES | SIGCHLD,
                          &ctx);
	}

  // join and clean up
  for (int i = 0; i < THREADS; i++) {
    waitpid(thread_ids[i], NULL, 0);
    free(stacks[i]);
  }

	tally_clean();

	printf( "Tally is %" PRIu64 "\n", tally );
	return 0;
}

