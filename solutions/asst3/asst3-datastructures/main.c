#include <stdlib.h>
#include <stdio.h>
#include "datastruct.h"
#include "../common/macros.h"

int main()
{
  ASSERT(front() == -1, "front() test 1");

  insert(1, 12);
  ASSERT(front() == 1, "insert() test 1");
  insert(2, 12);
  ASSERT(front() == 1, "insert() test 2");
  insert(3, 24);
  ASSERT(front() == 3, "insert() test 3");
  insert(4, 25);
  ASSERT(front() == 4, "insert() test 4");
  insert(5, 24);
  ASSERT(front() == 4, "insert() test 5");

  pop();
  ASSERT(front() == 3, "pop() test 1");
  pop();
  ASSERT(front() == 5, "pop() test 2");
  pop();
  ASSERT(front() == 1, "pop() test 3");
  pop();
  ASSERT(front() == 2, "pop() test 4");
  pop();
  ASSERT(front() == -1, "pop() test 5");

  insert(1, 25);
  insert(2, 12);
  insert(3, 24);
  insert(4, 26);
  popId(3);
  ASSERT(front() == 4, "popId() test 1");
  popId(4);
  ASSERT(front() == 1, "popId() test 1");
  popId(1);
  ASSERT(front() == 2, "popId() test 1");

	return 0;
}
