#define _GNU_SOURCE

#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

void server();
void client();


#define SHAREDMEM_SIZE 4096

// global context will be located
// in a single shared segment
typedef struct {
	unsigned int x;
	unsigned int y;
	sem_t s_child;
	sem_t s_parent;
	uint8_t terminate:1;
} shared_struct_t;

shared_struct_t* shared_struct;

int main()
{
	/* Don't remove this! We need it for testing. */
	setbuf( stdout, NULL );

	/* Erstellen eines neuen shared memory segments mit der Groesse SHAREDMEM_SIZE
	und Lese- (S_IRUSR) und Schreib-Berechtigungen (S_IWUSR)... */
	int identifier = shmget(IPC_PRIVATE, SHAREDMEM_SIZE, S_IRUSR|S_IWUSR);

	/* Das shared memory segments wird dem address space dieses Prozesses zugewiesen
	(SHared Memory ATtach). Die zugewiesene Adresse ist dabei egal (NULL) und soll automatisch
	gewaehlt werden, und es sollen keine besonderen Flags gesetzt werden (0), wir haben daher
	auch Schreibzugriff (mit dem Flag SHM_RDONLY haetten wir nur Lesezugriff). */
	shared_struct = shmat(identifier, NULL, 0);

	// Initialisiere die Variablen in shared_struct...
	shared_struct->x = 0;
	shared_struct->y = 0;
	shared_struct->terminate = 0;
	sem_init(&(shared_struct->s_child), 1, 0);
	sem_init(&(shared_struct->s_parent), 1, 0);

	pid_t pid = fork();

	if ( pid == 0 )
	{
		client();
	}
	else if ( pid > 0 )
	{
		server();
		waitpid( pid, NULL, 0 );
	}
	else if ( pid == -1 )
	{
		perror( "fork" );
	}

	sem_destroy(&(shared_struct->s_child));
	sem_destroy(&(shared_struct->s_parent));
	/* Das shared memory segment wird vom Adressraum des Prozesses abgeloest... */
	shmdt(shared_struct);

	return 0;
}

void server()
{
	while (1) {
		// warten, bis neue zahlen im shared memory sind
		sem_wait(&shared_struct->s_parent);

		if (shared_struct->terminate)
			break;

		printf("%lu\n", (unsigned long)shared_struct->x + (unsigned long)shared_struct->y);

		// dem client mitteilen, dass der server fertig ist
		sem_post(&shared_struct->s_child);
	}
}

void client()
{
	size_t nbytes = 1;
	char* line = malloc(nbytes+1);

	while (1) {
		getline(&line, &nbytes, stdin);

		if (strstr(line, "-1 -1") == line) {
			shared_struct->terminate = 1;
		}
		else if (sscanf(line, "%u %u", &shared_struct->x, &shared_struct->y) != 2) {
			continue;
		}

		// dem server mitteilen, dass neue zahlen im shared memory sind
		sem_post(&shared_struct->s_parent);

		if (shared_struct->terminate)
			break;

		// warten, bis der server fertig ist
		sem_wait(&shared_struct->s_child);
	}

	exit(EXIT_SUCCESS);
}
