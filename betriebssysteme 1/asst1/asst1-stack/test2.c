//#include <linux/module.h>
#include <linux/config.h>
#include <linux/init.h>
#include <stdint.h>

uint64_t square ( uint32_t x )
{
	uint64_t res;
	res = x*x;
	return res;
}


static int __init mymodule_init(void)
{
	uint32_t y = 5, z;
	z = square( y );
	return 0;
}

static void __exit mymodule_exit(void)
{
        return;
}

module_init(mymodule_init);
module_exit(mymodule_exit);

MODULE_LICENSE("GPL");
