	.text
square:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	movl	8(%ebp), %eax
	imull	8(%ebp), %eax
	movl	$0, %edx
	movl	%eax, -8(%ebp)
	movl	%edx, -4(%ebp)
	movl	-8(%ebp), %eax
	movl	-4(%ebp), %edx
	leave
	ret
main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-8, %esp
	subl	$24, %esp
	movl	$5, 20(%esp)
	movl	20(%esp), %eax
	movl	%eax, (%esp)
	call	square
	movl	%eax, 16(%esp)
	movl	$0, %eax
	leave
	ret
