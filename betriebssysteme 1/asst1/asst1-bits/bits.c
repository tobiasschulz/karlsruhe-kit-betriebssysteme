#include <stdint.h>

#include "bits.h"


unsigned int roundDown ( unsigned int addr )
{
	return addr & 0b11111111111111111111000000000000;
}


int getN ( uint8_t *A, unsigned int n )
{
	unsigned int index = (n - n % 8) / 8;
    unsigned int bit = 7 - n%8;
	unsigned int mask = 1 << bit;
	return (A[index] & mask) == 0 ? 0 : 1;
}

void setN ( uint8_t *A, unsigned int n )
{
	unsigned int index = (n - n % 8) / 8;
    unsigned int bit = 7 - n%8;
	unsigned int mask = 1 << bit;
    A[index] |= mask;
}

void clrN ( uint8_t *A, unsigned int n )
{
	unsigned int index = (n - n % 8) / 8;
    unsigned int bit = 7 - n%8;
	unsigned int mask = ~(1 << bit);
    A[index] &= mask;
}


void invert ( uint8_t *A, unsigned int n, unsigned int p )
{
	for (; n < p; ++n) {
		if (getN(A, n) == 0)
			setN(A, n);
		else
			clrN(A, n);
	}
}


void rot ( uint64_t *i, int n )
{
	uint64_t maskLsb = 1;
	uint64_t maskMsb = (uint64_t)1 << 63;

	if (n > 0) {
		for (; n != 0; --n) {
			uint64_t lsb = (*i & maskLsb);
			*i >>= 1;
			if (lsb != 0)
				*i |= maskMsb;
		}
	}
	else {
		for (; n != 0; ++n) {
			uint64_t msb = (*i & maskMsb);
			*i <<= 1;
			if (msb != 0)
				*i |= maskLsb;
		}
	}
}
