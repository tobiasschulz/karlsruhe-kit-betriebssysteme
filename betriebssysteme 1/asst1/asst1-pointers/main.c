#include "pointers.h"
#include <stdio.h>

int main()
{
	/*
	 * Put your test code here.
	 */

	int array[] = {1,2,3,4,5,6,7,8,9};
	int avg = average(array, 9);
	printf("%i\n", avg);

	int* array2[] = {&array[0], &array[1], &array[2], &array[3], &array[4], &array[5], &array[6], &array[7], &array[8]};
	int avg2 = p_average(array2, 9);
	printf("%i\n", avg2);


	return 0;
}
